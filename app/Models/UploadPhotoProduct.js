'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UploadPhotoProduct extends Model {
    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'media_files'
    }
  
    static get primaryKey () {
      return 'media_files_id'
    }

    static get searchField (){
      return 'media_files_tables_relations'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at", "media_files_noted", "media_files_keywords_relations", "media_files_tables_relations"];
    }

    static fillable(){
        return {
            created: [
                "media_files_id_relations",
                "media_files_data",
                "media_files_noted",
                "media_files_data_detail"
            ],
            updated: [
                "media_files_id_relations",
                "media_files_data",
                "media_files_noted",
                "media_files_data_detail"
            ],
            files: [
                "media_files_data"
            ],
            storeFile: [
                "media_files_data_detail"
            ]
        }
    }

    static validator(){
        return {
            created: {
                media_files_id_relations: "required|uuid|relations:products,products_id",
                media_files_data: "required|file|file_size:20mb"
            },
            updated: {
                media_files_id_relations: "required|uuid|relations:products,products_id",
                media_files_data: "required|file|file_size:20mb"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')

        this.addHook('beforeSave', (userInstance) => {
            userInstance.media_files_tables_relations = "products"
            userInstance.media_files_keywords_relations = "photo_product"
        })
    }
}

module.exports = UploadPhotoProduct
