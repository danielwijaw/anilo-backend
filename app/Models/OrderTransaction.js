'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');
const Product = use("App/Models/Product");

class OrderTransaction extends Model {
    static get incrementing () {
      return true
    }
    
    static get table () {
      return 'order_transactions'
    }
  
    static get primaryKey () {
      return 'order_transactions_id'
    }

    static get searchField (){
      return 'order_transactions_status'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static get StaticPaymentStatus(){
        return [
            {
                key_status: 'process',
                text_status: 'Customer Order Product'
            },
            {
                key_status: 'payment',
                text_status: 'Customer Submit Payment Proof'
            },
            {
                key_status: 'approve',
                approve: 'Order Transactions In Process'
            },
            {
                key_status: 'shipping',
                approve: 'Process Shipping'
            },
        ]
    }

    static fillable(){
        return {
            created: [
                "order_transactions_products_id",
                "order_transactions_qty",
                "order_transactions_discount",
                "order_transactions_users_detail"
            ],
            updated: [
                "order_transactions_products_id",
                // "order_transactions_qty",
                "order_transactions_discount",
                "order_transactions_users_detail"
            ],
        }
    }

    static validator(){
        return {
            created: {
                order_transactions_products_id: "required|uuid|relations:products,products_id",
                order_transactions_qty: "required|number|above:0",
                order_transactions_discount: "number|above:-1",
                order_transactions_users_detail: "required",
                'order_transactions_users_detail.customers_name': "required|max:180",
                'order_transactions_users_detail.customers_phone': "required|max:13",
                'order_transactions_users_detail.customers_mail': "required|email|max:80",
                'order_transactions_users_detail.customers_address': "required|max:500",
                'order_transactions_users_detail.customers_id': "uuid|relations:customers,customers_id"
            },
            updated: {
                order_transactions_products_id: "required|uuid|relations:products,products_id",
                // order_transactions_qty: "required|number|above:0",
                order_transactions_discount: "number|above:-1",
                order_transactions_users_detail: "required",
                'order_transactions_users_detail.customers_name': "required|max:180",
                'order_transactions_users_detail.customers_phone': "required|max:13",
                'order_transactions_users_detail.customers_mail': "required|email|max:80",
                'order_transactions_users_detail.customers_address': "required|max:500",
                'order_transactions_users_detail.customers_id': "uuid|relations:customers,customers_id"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')

        this.addHook('beforeCreate', async (userInstance) => {
            const productList = await Product.find(userInstance.order_transactions_products_id);
            if(!productList){
                throw new Error('Product Not Found');
            }
            const productListJson = productList.toJSON();
            if(Number(productListJson.products_qty) - Number(userInstance.order_transactions_qty) < 1){
                throw new Error('Qty Product Not Enough');
            }

            userInstance.order_transactions_price = Number(productListJson.products_price) * (userInstance.order_transactions_qty)

            const updateProductQty = await Product.query().where('products_id', userInstance.order_transactions_products_id).update({
                products_qty: Number(productListJson.products_qty) - Number(userInstance.order_transactions_qty)
            }); 
        })

        this.addHook('beforeSave', async (userInstance) => {
            if(userInstance.order_transactions_users_detail){
                userInstance.order_transactions_users_detail = JSON.stringify(userInstance.order_transactions_users_detail);
            }
        })
    }

    payment_image () {
      return this.hasOne('App/Models/UploadPhotoPayment', 'order_transactions_id', 'media_files_id_relations')
    }
}

module.exports = OrderTransaction
