'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Admin extends Model {
    static get incrementing () {
        return true
    }
    
    static get table () {
      return 'admins'
    }
  
    static get primaryKey () {
      return 'admins_id'
    }

    static get searchField (){
      return 'admins_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static fillable(){
        return {
            created: [
                "admins_name"
            ],
            updated: [
                "admins_name"
            ],
        }
    }

    static validator(){
        return {
            created: {
                admins_name: "required|max:150"
            },
            updated: {
                admins_name: "required|max:150"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }
}

module.exports = Admin
