'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  
  static get primaryKey () {
    return 'users_id'
  }

  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.users_password) {
        userInstance.users_password = await Hash.make(userInstance.users_password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  admins_data() {
    return this.belongsTo('App/Models/Admin', 'users_admin_id', 'admins_id')
  }

  customers_data() {
    return this.belongsTo('App/Models/Customer', 'users_customer_id', 'customers_id')
  }
}

module.exports = User
