'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Customer extends Model {
    static get incrementing () {
      return true
    }
    
    static get table () {
      return 'customers'
    }
  
    static get primaryKey () {
      return 'customers_id'
    }

    static get searchField (){
      return 'customers_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at"];
    }

    static fillable(){
        return {
            created: [
                "customers_name",
                "customers_phone",
                "customers_mail",
                "customers_address"
            ],
            updated: [
                "customers_name",
                "customers_phone",
                "customers_mail",
                "customers_address"
            ],
        }
    }

    static validator(){
        return {
            created: {
                customers_name: "required|max:180",
                customers_phone: "required|max:13",
                customers_mail: "required|email|max:80",
                customers_address: "required|max:500"
            },
            updated: {
                customers_name: "required|max:180",
                customers_phone: "required|max:13",
                customers_mail: "required|email|max:80",
                customers_address: "required|max:500"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }
}

module.exports = Customer
