'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
    static get incrementing () {
      return true
    }
    
    static get table () {
      return 'products'
    }
  
    static get primaryKey () {
      return 'products_id'
    }

    static get searchField (){
      return 'products_name'
    }

    static get hidden() {
      return ["created_at", "updated_at", "deleted_at", "products_image"];
    }

    static fillable(){
        return {
            created: [
                "products_name",
                "products_qty",
                "products_price"
            ],
            updated: [
                "products_name",
                "products_qty",
                "products_price"
            ],
        }
    }

    static validator(){
        return {
            created: {
                products_name: "required|max:180",
                products_qty: "required|number|above:0",
                products_price: "required|number|above:0"
            },
            updated: {
                products_name: "required|max:180",
                products_qty: "required|number|above:0",
                products_price: "required|number|above:0"
            },
        }
    }

    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }

    products_image () {
      return this.hasOne('App/Models/UploadPhotoProduct', 'products_id', 'media_files_id_relations')
    }
}

module.exports = Product
