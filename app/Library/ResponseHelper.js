'use strict'

module.exports = function(params = {}){
    var returnJson = {
        responseCode: 200,
        status: true,
        message: "",
        data: [],
        error: null
    }

    for(var keyParams in params){
        for(var keyJson in returnJson){
            if(keyParams == keyJson && params[keyParams]){
                returnJson[keyJson] = params[keyParams]
            }
        }
    }

    return {
        head: {
            responseCode: returnJson.responseCode,
            message: returnJson.message,
            returnDate: new Date()
        },
        body: {
            status: String(returnJson.status),
            message: returnJson.message,
            data: returnJson.data
        },
        error: returnJson.error
    }
}