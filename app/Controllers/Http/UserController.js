'use strict'

const User = use("App/Models/User");
const Token = use("App/Models/Token");
const { validateAll } = use("Validator");
const ResponseHelper = require("../../Library/ResponseHelper");
const Hash = use("Hash");
const WriteTmpLog = require("../../Library/WriteTmpLog");
const Admin = use("App/Models/Admin");

class UserController {
    constructor() {
        this.nameModel = "Users"
        this.relationsNameModel = this.nameModel;
    }

    async generateAccount({response}){
        const usersAccount = await User.query().first();

        if(usersAccount){
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Account Already Exist',
                    error: {
                        message: 'Account Already Exist',
                        stack: 'Account Already Exist'
                    }
                })
            )
        }

        const insertAdmin = await Admin.create({
            admins_name: "Admin"
        })

        const userAccount = await User.create({
            users_username: 'admin1',
            users_password: 'admin12345',
            users_admin_id: insertAdmin.admins_id
        });
        
        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: "User Admin Success Created",
            data: {
                users_username: 'admin1',
                users_password: 'admin12345'
            }
        }));

    }

    async registerCustomer({ auth, request, response }) {
        const rules = {
            users_username: "required|exists:users,users_username",
            users_password: "required|min:8",
            users_customer_id: "required|uuid|relations:customers,customers_id"
        };

        const validation = await validateAll(request.all(), rules);

        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })
            
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const userData = request.only(['users_username', 'users_password', 'users_customer_id']);
        const user = await User.create({
            users_username: userData.users_username,
            users_password: userData.users_password,
            users_customer_id: userData.users_customer_id
        });

        const authedUser = await auth.attempt(userData.users_username, userData.users_password)
        
        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: "User Customers Success Created",
            data: authedUser
        }));
    }

    async registerAdmins({ auth, request, response }) {
        const rules = {
            users_username: "required|exists:users,users_username",
            users_password: "required|min:8",
            users_admin_id: "required|uuid|relations:admins,admins_id"
        };

        const validation = await validateAll(request.all(), rules);

        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })
            
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const userData = request.only(['users_username', 'users_password', 'users_admin_id']);
        const user = await User.create({
            users_username: userData.users_username,
            users_password: userData.users_password,
            users_admin_id: userData.users_admin_id
        });

        const authedUser = await auth.attempt(userData.users_username, userData.users_password)
        
        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: "User Admin Success Created",
            data: authedUser
        }));
    }

    async login({ auth, request, response }) {
        const rules = {
            users_username: "required",
            users_password: "required|min:8"
        };

        const validation = await validateAll(request.all(), rules);

        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })
            
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const userData = request.only(['users_username', 'users_password']);

        const userLogin = await User.query().where({users_username: userData.users_username}).whereNull('deleted_at')
        .with('admins_data')
        .with('customers_data')
        .first()

        if(!userLogin) {
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 404,
                    status: "false",
                    message: 'Users not Found',
                    error: {
                        message: "Users not Found",
                        stack: "Users not Found"
                    }
                })
            )
        }

        const passwordsMatch = await Hash.verify(userData.users_password, userLogin.users_password)

        if(!passwordsMatch) {
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Password is Wrong',
                    error: {
                        message: {
                            password: "Wrong"
                        },
                        stack: {
                            password: "Wrong"
                        }
                    }
                })
            )
        }

        const authedUser = await auth.attempt(userData.users_username, userData.users_password)

        WriteTmpLog({
            directoryLog: 'login/',
            nameLog: 'login',
            fileData: {
                date: new Date(),
                requestUrl: request.url(),
                userRequest: {
                    ip: request.ip()+" || "+request.ips(),
                    hostname: request.hostname(),
                    originalUrl: request.originalUrl(),
                    method: request.method(),
                    header: {
                    "User-Agent": request.header('User-Agent'),
                    Host: request.header('User-Agent'),
                    Authorization: request.header('Authorization'),
                    "Content-Type": request.header('Content-Type'),
                    Accept: request.header('Accept')
                    }
                },
                response: authedUser
            }
        })

        var dataLogin = "";

        const userLoginJson = userLogin.toJSON();

        if(userLoginJson.admins_data){
            dataLogin = " Admin Access"
        }

        if(userLoginJson.customers_data){
            dataLogin = " Customer Access"
        }

        return response.status(200).json(ResponseHelper({
            responseCode: 201,
            message: "User success login" + dataLogin,
            data: authedUser
        }));
    }

    async logout({auth, request, response}){
        const user = await auth.user;

        const tokenList = await Token.query().where('user_id', user.users_id).fetch()

        WriteTmpLog({
            directoryLog: 'logout/',
            nameLog: 'logout',
            fileData: {
                date: new Date(),
                requestUrl: request.url(),
                userRequest: {
                    ip: request.ip()+" || "+request.ips(),
                    hostname: request.hostname(),
                    originalUrl: request.originalUrl(),
                    method: request.method(),
                    header: {
                    "User-Agent": request.header('User-Agent'),
                    Host: request.header('User-Agent'),
                    Authorization: request.header('Authorization'),
                    "Content-Type": request.header('Content-Type'),
                    Accept: request.header('Accept')
                    },
                    body: request.all()
                },
                response: {
                    message: "Users logout",
                    tokenList: tokenList
                }
            }
        })

        return response.status(200).json(
            ResponseHelper({
                responseCode: 200,
                message: 'Token Revoked'
            })
        )
    }
}

module.exports = UserController
