const OrderTransaction = use("App/Models/OrderTransaction");
const ApiController = require("../../../app/Library/ApiController");
const { validateAll } = use("Validator");
const ResponseHelper = require("../../../app/Library/ResponseHelper");
const WriteTmpLog = require("../../../app/Library/WriteTmpLog");
const moment = use("moment");
const fs = use("fs");
const UploadPhotoPayment = use("App/Models/UploadPhotoPayment");

class OrderTransactionController extends ApiController {
    constructor() {
        super()
        this.modelData = OrderTransaction;
        this.nameModel = "Order Transaction List";
        this.relationsNameModel = this.nameModel;
    }

    async listStatusTransactions({ auth, request, response }){
        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: "List Order Transaction Status",
            data: this.modelData.StaticPaymentStatus
        }));
    }

    async index({ auth, request, response }) {
        var page = request.get().page || 1+':'+20
        var page = page.split(':')

        var order = request.get().order || 'created_at:desc'
        var order = order.split(':')

        var search = request.get().search
        var where = request.get().where

        var listData = this.modelData.query()
        this.generateSearchModelData(listData, request.get().search)
        this.generateWhereModelData(listData, request.get().where)
        listData.with('payment_image', (builder) => {
            builder.select('media_files_id', 'media_files_id_relations')
            .orderBy('created_at', 'DESC')
        })
        listData.orderBy(order[0], order[1] || 'desc')

        listData = await listData.paginate(page[0], page[1] || 20)
        listData = listData.toJSON()

        for(var list in listData['data']){
            listData['data'][list]['order_transactions_status'] = this.modelData.StaticPaymentStatus.filter(keystat => {
                return keystat.key_status == listData['data'][list]['order_transactions_status']
            })[0]
            if(listData['data'][list]['payment_image'] && listData['data'][list]['payment_image']['media_files_id']){
                listData['data'][list]['payment_image'] = '/api/v1/payment-images/'+listData['data'][list]['payment_image']['media_files_id']
            }
        }

        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: this.nameModel+" All",
            data: listData
        }));
    }

    async UploadPaymentCustomer({auth, request, response}){

        const rules = UploadPhotoPayment.validator().created;
        const fileInput = request.only(UploadPhotoPayment.fillable().created);
        fileInput[UploadPhotoPayment.fillable().files[0]] = request.file(UploadPhotoPayment.fillable().files[0]);
        const validation = await validateAll(fileInput, rules);
        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })
            
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const bufferFile = fs.readFileSync(fileInput[UploadPhotoPayment.fillable().files[0]].tmpPath);

        fileInput[UploadPhotoPayment.fillable().storeFile[0]] = fileInput[UploadPhotoPayment.fillable().files[0]];

        fileInput[UploadPhotoPayment.fillable().files[0]] = bufferFile;

        const processInsert = await UploadPhotoPayment.create(fileInput);

        processInsert[UploadPhotoPayment.fillable().files[0]] = "";

        const updateData = await this.modelData.query().where(this.modelData.primaryKey, request.all().media_files_id_relations).update({
            order_transactions_status: 'payment'
        });

        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: this.nameModel+" success Created",
            data: processInsert
        }));

    }

    async paymentApprove({request, response}){
        const rules = {
            'order_transactions_id':"required|uuid|relations:order_transactions,order_transactions_id"
        }
        const validation = await validateAll(request.all(), rules);
        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })

            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const getOrderTrx = await this.modelData.query().where(this.modelData.primaryKey, request.all().order_transactions_id).first()
        const getOrderTrxJson = getOrderTrx.toJSON();

        if( getOrderTrxJson.order_transactions_status != 'payment' ){
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: 'Customer not yet upload payment / is approve',
                    errorName: 'Customer not yet upload payment / is approve',
                    errorStack: 'Customer not yet upload payment / is approve'
                }
            })

            return response.status(406).json(
                ResponseHelper({
                    responseCode: 406,
                    status: "false",
                    message: 'Customer not yet upload payment / is approve',
                    error: {
                        message: 'Customer not yet upload payment / is approve',
                        stack: 'Customer not yet upload payment / is approve'
                    }
                })
            )
        }

        const updateData = await this.modelData.query().where(this.modelData.primaryKey, request.all().order_transactions_id).update({
            order_transactions_payed_at: moment().format('YYYY-MM-DD HH:mm:ss'),
            order_transactions_status: 'approve'
        });
        
        return response.status(201).json(
            ResponseHelper({
                responseCode: 201,
                status: "true",
                message: 'Payment is Approve'
            })
        )
    }

    async shippingUpdate({request, response}){
        const rules = {
            'order_transactions_id':"required|uuid|relations:order_transactions,order_transactions_id",
            'order_transactions_shipping_id':"required|max:60"
        }
        const validation = await validateAll(request.all(), rules);
        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })

            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const getOrderTrx = await this.modelData.query().where(this.modelData.primaryKey, request.all().order_transactions_id).first()
        const getOrderTrxJson = getOrderTrx.toJSON();

        if( getOrderTrxJson.order_transactions_status != 'approve' ){
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: 'Status Payment is not approve',
                    errorName: 'Status Payment is not approve',
                    errorStack: 'Status Payment is not approve'
                }
            })

            return response.status(406).json(
                ResponseHelper({
                    responseCode: 406,
                    status: "false",
                    message: 'Status Payment is not approve',
                    error: {
                        message: 'Status Payment is not approve',
                        stack: 'Status Payment is not approve'
                    }
                })
            )
        }

        const updateData = await this.modelData.query().where(this.modelData.primaryKey, request.all().order_transactions_id).update({
            order_transactions_shipping_id: request.all().order_transactions_shipping_id,
            order_transactions_status: 'shipping'
        });
        
        return response.status(201).json(
            ResponseHelper({
                responseCode: 201,
                status: "true",
                message: 'Transactions In Process Shipping'
            })
        )
    }

    async store({ auth, request, response }) {
        const rules = this.modelData.validator().created;
        const validation = await validateAll(request.all(), rules);
        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })

            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        var listData = request.only(this.modelData.fillable().created);

        if(auth.user && this.modelData.fillable().created.includes('created_by')){
            listData.created_by = auth.user.users_admin_id
        }

        if(auth && auth.user){
            listData.order_transactions_users_detail.customers_id = auth.user.users_customer_id
        }

        const processInsert = await this.modelData.create(listData);

        if(auth && auth.user){
            const usersId = auth.user.users_admin_id;
            const employeeData = await AdminModels.getRowData(usersId);

            const logUser = new HistoryUser();
            logUser.setup_log_activities_models = this.modelData.table
            logUser.setup_log_activities_activity = 'Added New Record'
            logUser.setup_log_activities_users = employeeData
            logUser.setup_log_activities_old_data = processInsert
            logUser.setup_log_activities_newest_data = processInsert
            await logUser.save()
        }

        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: this.nameModel+" success Created",
            data: processInsert
        }));
    }
}

module.exports = OrderTransactionController
