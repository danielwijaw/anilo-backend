const Customer = use("App/Models/Customer");
const ApiController = require("../../../app/Library/ApiController");

class CustomerController extends ApiController {
    constructor() {
        super()
        this.modelData = Customer;
        this.nameModel = "Customer List";
        this.relationsNameModel = this.nameModel;
    }
}

module.exports = CustomerController
