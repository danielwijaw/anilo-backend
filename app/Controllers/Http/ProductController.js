'use strict'

const Product = use("App/Models/Product");
const ApiController = require("../../../app/Library/ApiController");
const ResponseHelper = require("../../../app/Library/ResponseHelper");

class ProductController extends ApiController {
    constructor() {
        super()
        this.modelData = Product;
        this.nameModel = "Product List";
        this.relationsNameModel = this.nameModel;
    }

    async index({ auth, request, response }) {
        var page = request.get().page || 1+':'+20
        var page = page.split(':')

        var order = request.get().order || 'created_at:desc'
        var order = order.split(':')

        var search = request.get().search
        var where = request.get().where

        var listData = this.modelData.query()
        this.generateSearchModelData(listData, request.get().search)
        this.generateWhereModelData(listData, request.get().where)
        listData.with('products_image', (builder) => {
            builder.select('media_files_id', 'media_files_id_relations')
            .orderBy('created_at', 'DESC')
        })
        listData.orderBy(order[0], order[1] || 'desc')

        listData = await listData.paginate(page[0], page[1] || 20)
        listData = listData.toJSON()

        for(var list in listData['data']){
            if(listData['data'][list]['products_image'] && listData['data'][list]['products_image']['media_files_id']){
                listData['data'][list]['products_image'] = '/api/v1/product-images/'+listData['data'][list]['products_image']['media_files_id']
            }
        }

        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: this.nameModel+" All",
            data: listData
        }));
    }
}

module.exports = ProductController
