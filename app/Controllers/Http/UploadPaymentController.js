'use strict'
const ApiController = require("../../../app/Library/ApiController");
const ResponseHelper = require("../../../app/Library/ResponseHelper");
const { validateAll } = use("Validator");
const UploadPhotoPayment = use("App/Models/UploadPhotoPayment");
const fs = use("fs")

class UploadPaymentController extends ApiController {

    constructor() {
        super()
        this.modelData = UploadPhotoPayment;
        this.nameModel = "Upload Photo Payment List";
        this.relationsNameModel = this.nameModel;
    }

    async uploadMedias({auth, request, response}){

        const rules = this.modelData.validator().created;
        const fileInput = request.only(this.modelData.fillable().created);
        fileInput[this.modelData.fillable().files[0]] = request.file(this.modelData.fillable().files[0]);
        const validation = await validateAll(fileInput, rules);
        if (validation.fails()) {
            await WriteTmpLog({
                directoryLog: 'error/',
                nameLog: 'error-log',
                fileData: {
                    date: new Date(),
                    requestUrl: request.url(),
                    userRequest: {
                        ip: request.ip()+" || "+request.ips(),
                        hostname: request.hostname(),
                        originalUrl: request.originalUrl(),
                        method: request.method(),
                        header: {
                        "User-Agent": request.header('User-Agent'),
                        Host: request.header('User-Agent'),
                        Authorization: request.header('Authorization'),
                        "Content-Type": request.header('Content-Type'),
                        Accept: request.header('Accept')
                        },
                        body: request.all()
                    },
                    errorMessage: validation.messages(),
                    errorName: 'Error Validations',
                    errorStack: validation.messages()
                }
            })
            
            return response.status(400).json(
                ResponseHelper({
                    responseCode: 400,
                    status: "false",
                    message: 'Some Error in Validation',
                    error: {
                        message: validation.messages(),
                        stack: validation.messages()
                    }
                })
            )
        }

        const bufferFile = fs.readFileSync(fileInput[this.modelData.fillable().files[0]].tmpPath);

        fileInput[this.modelData.fillable().storeFile[0]] = fileInput[this.modelData.fillable().files[0]];

        fileInput[this.modelData.fillable().files[0]] = bufferFile;

        const processInsert = await this.modelData.create(fileInput);

        processInsert[this.modelData.fillable().files[0]] = "";

        return response.status(201).json(ResponseHelper({
            responseCode: 201,
            message: this.nameModel+" success Created",
            data: processInsert
        }));

    }

    async showData({ auth, params, response }) {

        const findData = await this.modelData.find(params.id);

        if (!findData) {
            return response.status(404).json(ResponseHelper({
                responseCode: 404,
                status: "false",
                message: this.nameModel+' Not Found',
            }));
        }

        return response.type(findData[this.modelData.fillable().storeFile[0]].headers["content-type"]).send(findData[this.modelData.fillable().files[0]])
    }
}

module.exports = UploadPaymentController
