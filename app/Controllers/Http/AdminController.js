const Admin = use("App/Models/Admin");
const ApiController = require("../../../app/Library/ApiController");

class AdminController extends ApiController {
    constructor() {
        super()
        this.modelData = Admin;
        this.nameModel = "Admin List";
        this.relationsNameModel = this.nameModel;
    }
}

module.exports = AdminController
