'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const ResponseHelper = require("../app/Library/ResponseHelper")

Route.group(() => {
  Route.get("product-list", "ProductController.index");
  Route.get("product-list/:id", "ProductController.show");
  Route.post("product-list", "ProductController.store");
  Route.resource("product", "ProductController").apiOnly().middleware('auth');
  Route.post("product-images", "UploadMediaController.uploadMedias");
  Route.get("product-images/:id", "UploadMediaController.showData");

  Route.resource("customer", "CustomerController").apiOnly().middleware('auth');
  Route.post("customers-register", "UserController.registerCustomer").middleware('auth');

  Route.resource("admin", "AdminController").apiOnly().middleware('auth');
  Route.post("admin-register", "UserController.registerAdmins").middleware('auth');

  Route.post("order-transactions", "OrderTransactionController.store");
  Route.resource("order-transaction", "OrderTransactionController").apiOnly().middleware('auth');
  Route.get("list-status", "OrderTransactionController.listStatusTransactions");
  Route.post("upload-payment", "OrderTransactionController.UploadPaymentCustomer").middleware('auth');
  Route.post("verify-payment", "OrderTransactionController.paymentApprove").middleware('auth');
  Route.get("payment-images/:id", "UploadPaymentController.showData");
  Route.post("shipping-transactions", "OrderTransactionController.shippingUpdate").middleware('auth');

  Route.post("login-access", "UserController.login");
  Route.post("logout-access", "UserController.logout").middleware('auth');

  Route.post("generate-account", "UserController.generateAccount");
}).prefix("api/v1")


Route.get('*', () => {
  return ResponseHelper({
    message: "Anilo API"
  })
})