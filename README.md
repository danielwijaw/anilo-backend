# ANILO BACKEND SERVICE

Using Technology Javascript Engine Chrome v8 (nodeJS) & Framework Adonis.JS 4.1

**MAKE SURE READ THE NOTED !!!**

## LINK
1. POSTMAN URL : https://www.getpostman.com/collections/2dc394caf1305b346f29
2. Apps URL : https://anilo-api.visitmise.com

## Setup Application 

1. Clone this Repository
```bash
git clone https://gitlab.com/danielwijaw/anilo-backend.git
```
2. Install Requirement Dependency
```bash
npm install
```
3. Create *.env* base on *.env.example*
```bash
cp .env.example .env
```
4. Setup Database in *.env*
5. Run Migration
```bash
adonis run:migration
```
6. Serve Adonis
```bash
adonis serve --dev
```

## Flow Applications
1. Collection POSTMAN -> Generate Account
2. Collection POSTMAN -> Login Access
3. Collection POSTMAN -> Product
4. Collection POSTMAN -> Order Transactions

## NOTED
This apps using
1. NodeJS **v12.19.1**
2. NPM **v6.14.8**
3. PostgreSQL **13.1**
4. PostgreSQL Contrib Package
5. Adonis CLI **4.0.12**
