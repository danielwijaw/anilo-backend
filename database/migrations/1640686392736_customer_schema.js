'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.uuid('customers_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('customers_name', 180).notNullable()
      table.string('customers_phone', 13).notNullable()
      table.string('customers_mail', 80).notNullable()
      table.text('customers_address').notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomerSchema
