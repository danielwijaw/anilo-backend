'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddedFieldShippingIdOrderTransactionsSchema extends Schema {
  up () {
    this.table('order_transactions', (table) => {
      table.string('order_transactions_shipping_id', 60).nullable()
    })
  }

  down () {
    this.table('order_transactions', (table) => {
      table.dropColumn('order_transactions_shipping_id')
    })
  }
}

module.exports = AddedFieldShippingIdOrderTransactionsSchema
