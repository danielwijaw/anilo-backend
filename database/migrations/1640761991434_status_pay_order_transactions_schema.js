'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatusPayOrderTransactionsSchema extends Schema {
  up () {
    this.table('order_transactions', (table) => {
      table.timestamp('order_transactions_payed_at').nullable()
    })
  }

  down () {
    this.table('order_transactions', (table) => {
      table.dropColumn('order_transactions_payed_at')
    })
  }
}

module.exports = StatusPayOrderTransactionsSchema
