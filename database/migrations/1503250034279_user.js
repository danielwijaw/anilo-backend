'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.uuid('users_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('users_username', 80).notNullable().unique()
      table.string('users_password', 60).notNullable()
      table.uuid('users_customer_id').nullable()
      table.uuid('users_admin_id').nullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
