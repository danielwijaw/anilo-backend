'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdminSchema extends Schema {
  up () {
    this.create('admins', (table) => {
      table.uuid('admins_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('admins_name', 150).notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('admins')
  }
}

module.exports = AdminSchema
