'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MediaFileSchema extends Schema {
  up () {
    this.create('media_files', (table) => {
      table.uuid('media_files_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('media_files_id_relations')
      table.string('media_files_tables_relations', 30)
      table.string('media_files_keywords_relations', 30)
      table.binary('media_files_data').notNullable()
      table.json('media_files_data_detail').notNullable()
      table.text('media_files_noted').nullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('media_files')
  }
}

module.exports = MediaFileSchema
