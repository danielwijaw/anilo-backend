'use strict'

const Schema = use('Schema')
const Database = use('Database')

class SetupDbSchema extends Schema {
  async up () {
    await Database.raw(`
      CREATE TRIGGER setup_log_activities
        BEFORE UPDATE
        ON setup_log_activities
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
    await Database.raw(`
      CREATE TRIGGER admins
        BEFORE UPDATE
        ON admins
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
    await Database.raw(`
      CREATE TRIGGER customers
        BEFORE UPDATE
        ON customers
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
    await Database.raw(`
      CREATE TRIGGER products
        BEFORE UPDATE
        ON products
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
    await Database.raw(`
      CREATE TRIGGER order_transactions
        BEFORE UPDATE
        ON order_transactions
        FOR EACH ROW
        EXECUTE PROCEDURE upd_timestamp_users();
    `)
  }

  down () {
    
  }
}

module.exports = SetupDbSchema