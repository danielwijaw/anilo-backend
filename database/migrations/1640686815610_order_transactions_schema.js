'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderTransactionsSchema extends Schema {
  up () {
    this.create('order_transactions', (table) => {
      table.uuid('order_transactions_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('order_transactions_products_id').references('products_id').inTable('products')
      table.integer('order_transactions_qty').notNullable().defaultTo('0')
      table.string('order_transactions_status', '10').notNullable().defaultTo('process')
      table.float('order_transactions_price').notNullable().defaultTo('0')
      table.float('order_transactions_discount').notNullable().defaultTo('0')
      table.json('order_transactions_users_detail').notNullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('order_transactions')
  }
}

module.exports = OrderTransactionsSchema
