'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.uuid('products_id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.string('products_name', 180).notNullable()
      table.integer('products_qty').notNullable().defaultTo('0')
      table.float('products_price').notNullable().defaultTo('0')
      table.text('products_image').nullable()
      table.timestamp('created_at').defaultTo('NOW()')
      table.timestamp('updated_at').defaultTo('NOW()')
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
